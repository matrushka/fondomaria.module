(function($) {
  $(function() {
    var nodesApoyoSelectors = ['body.page-node-1', 'body.page-node-225'];
    var $forms = $(nodesApoyoSelectors.join());
    if($(nodesApoyoSelectors.join()).length == 1) {
      uiSolicitudApoyo();
    }
  })

  function uiSolicitudApoyo() {
    var $tabla = $('#edit-pruebas_embarazo-0-table');

    var $label1 = $('#edit-submitted-pruebas-embarazo-1-1');
    var $label2 = $('#edit-submitted-pruebas-embarazo-2-1');
    var $label3 = $('#edit-submitted-pruebas-embarazo-3-1');

    var $label = $label1.find('label').clone();

    $label1
      .empty()
      .append($label.clone())
      .find('label')
        .text('Orina')
        .removeClass('element-invisible');

    $label2
      .empty()
      .append($label.clone())
      .find('label')
        .text('Sangre')
        .removeClass('element-invisible');


    $label3
      .empty()
      .append($label.clone())
      .find('label')
        .text('Ultrasonido')
        .removeClass('element-invisible');

    // Disable semanas
    var $semanas1 = $('.form-item-submitted-pruebas-embarazo-1-3 input');
    $semanas1.remove();
  }
})(jQuery)
