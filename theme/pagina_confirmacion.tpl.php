<p>Muchas gracias por tu información, te pedimos que llenes el formulario que está en el vínculo que aparece debajo para poder saber más sobre tu experiencia antes de que te contactemos. <strong>Tu número de registro es <?php print $sid; ?></strong></p>
<p>
En estos vínculos puedes encontrar la información que solicitaste:
<ul>
<?php
foreach($links_informacion as $link) {
  $url = $link['url'];
  $texto = $link['text'];
  print "<li><a href='$url'>$texto</a></li>";
}
?>
</ul>
</p>
<p>&nbsp;</p>
<p>También te enviamos por correo (si nos diste uno) los vínculos y tu número de registro.</p>
