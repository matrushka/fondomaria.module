<?php


function fondomaria_admin_get_mapped($form, &$form_state) {
  $mapping_table = fondomaria_admin_get_mapping_array();
  $case_data = array();

  $missing_functions = '';
  
  foreach($mapping_table as $field) {
    $method = $field['extract_method'];

    // Get the extraction function based on the field type
    if($method == 'special') {
      $extract_function = 'fondomaria_admin_extract_special__' . $field['target_field_name'];
    }
    else {
      $extract_function = 'fondomaria_admin_extract_' . $field['extract_method'];
    }
    
    // If the extraction function exists, run it to get the field value
    // otherwise show a warning
    if(function_exists($extract_function)) {
      $value = $extract_function($field, $form_state);

      $target_field = $field['target_field_name'];

      // If the value is an array it means that from a single
      // webform component, we're going to populate several target fields
      // (it is likely that this will only be used for the phone fields)
      if(is_array($value)) {
        $target_fields = explode('+', $target_field);
        
        foreach($target_fields as $field_index=>$field_name) {
          $case_data[$field_name] = $value[$field_index];
        }

      }
      else {
        if(!is_null($value)) {
          $case_data[$target_field] = $value;
        }
      }
    }
    else {
      drupal_set_message('Función de extracción de campo indefinida: ' . $extract_function, 'warning', TRUE);

      if($method == 'special') {
$fn = <<<FUNCTION
function $extract_function(\$field, \$form_state) {<br>
&nbsp;&nbsp;//Function body here<br>
}<br><br>
FUNCTION;
        $missing_functions .= $fn;
      }
    }
  }

  drupal_set_message($missing_functions);

  return $case_data;
}

function fondomaria_admin_get_mapping_array() {
  $csv_path = realpath( drupal_get_path('module', 'fondomaria') .'/data/mapeo.csv' );
  $map_unfiltered = array_map('str_getcsv', file($csv_path));
  array_shift($map_unfiltered); // remove the headers
  $map_filtered = array();

  foreach($map_unfiltered as $row) {
    if(empty($row[7])) { // If there isn't a target field, skip the row
      continue;
    }

    array_push($map_filtered, array(
      'target_field_name' => $row[7],
      'source_component' => $row[8],
      'extract_method' => $row[9],
      'notes' => $row[6]
    ));
  }

  return $map_filtered;

}

function fondomaria_admin_send_case($values) {
  $fields_urlencoded = http_build_query($values);

  $url = 'http://staging.fondomaria.org/public/store';

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_urlencoded);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $headers = array('Content-Type: application/x-www-form-urlencoded');
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  
  $response = curl_exec($ch);
  $info = curl_getinfo($ch);

  curl_close($ch);

  $response_data = json_decode($response);
  $case_id = $response_data->response->id;

  return $case_id;
}

function fondomaria_date_from_array($date_array) {
  $year = $date_array['year'];
  $month = str_pad($date_array['month'], 2, '0', STR_PAD_LEFT);
  $day = str_pad($date_array['day'], 2, '0', STR_PAD_LEFT);

  $value = "$year-$month-$day";

  return $value;
}

function fondomaria_admin_get_input_field($field, $form_state) {
  $source = $field['source_component'];

  if(empty($source)) {
    drupal_set_message('Source component not set for target field ' . $field['target_field_name'], 'warning', FALSE);
    return NULL;
  }

  $input = $form_state['input']['submitted'];
  $input_field = $input[$source];

  return $input_field;
}

function fondomaria_admin_get_input_subfield($field, $form_state) {
  $source_string = $field['source_component'];

  if(empty($source_string)) {
    drupal_set_message('Source component not set for target field ' . $field['target_field_name'], 'warning', FALSE);
    return NULL;
  }

  if(strpos($source_string, ':') == FALSE) {
    return $form_state['input']['submitted'][$source_string];
  }

  $source_ids = explode(':', $source_string);
  $group_id = $source_ids[0];
  $field_id = $source_ids[1];

  return $form_state['input']['submitted'][$group_id][$field_id];
}


function fondomaria_admin_extract_text($field, $form_state) {
  return fondomaria_admin_get_input_field($field, $form_state);
}

function fondomaria_admin_extract_integer($field, $form_state) {
  return fondomaria_admin_extract_text($field, $form_state);
}

function fondomaria_admin_extract_select($field, $form_state) {
  return fondomaria_admin_extract_text($field, $form_state);
}

function fondomaria_admin_extract_radio($field, $form_state) {
  return fondomaria_admin_extract_text($field, $form_state);
}

function fondomaria_admin_extract_fixed_value($field, $form_state) {
  return $field['source_component'];
}

function fondomaria_admin_extract_date($field, $form_state) {
  $input_field = fondomaria_admin_get_input_field($field, $form_state);

  if(!$input_field) return NULL;

  return fondomaria_date_from_array($input_field);

}

function fondomaria_admin_extract_phone($field, $form_state) {
  $text_value = fondomaria_admin_get_input_field($field, $form_state);

  // Clean the text keeping only digits.
  $phone = preg_replace('/\D+/', '', $text_value);
  $length = mb_strlen($phone);

  if($length <= 8) { // Not enough numbers for an area code
    return array('', $phone);
  }
  else if($length <=12) {
    $lada_length = $length - 8;
    return array(
      mb_substr($phone, 0, $lada_length),
      mb_substr($phone, $lada_length, $length-$lada_length)
    );
  }
  else {
    return array(
      mb_substr($phone, 0, 4),
      mb_substr($phone, 4)
    );
  }
}

function fondomaria_admin_extract_boolean($field, $form_state) {
  $field = fondomaria_admin_get_input_subfield($field, $form_state);
  return (is_null($field)) ? NULL : '1';
}

function fondomaria_admin_extract_boolean_inverted($field, $form_state) {
  $value = fondomaria_admin_extract_boolean($field, $form_state);
  return (is_null($value)) ? '1' : NULL;
}

function fondomaria_admin_extract_special__edad_anios($field, $form_state) {
  $date = fondomaria_admin_extract_date($field, $form_state);

  $timestamp = strtotime($date);
  $now = time();
  $difference = $now - $timestamp;
  $years = floor($difference / 31536000);

  return $years;
}

function fondomaria_admin_extract_special__semanas($field, $form_state) {
  $date = fondomaria_admin_extract_date($field, $form_state);
  $timestamp = strtotime($date);
  $now = time();
  $difference = $now - $timestamp;
  $days = floor($difference / 86400);
  
  $weeks = floor($days / 7);
  return $weeks;
}

function fondomaria_admin_extract_special__dias($field, $form_state) {
  $date = fondomaria_admin_extract_date($field, $form_state);
  $timestamp = strtotime($date);
  $now = time();
  $difference = $now - $timestamp;
  $days = floor($difference / 86400);
  
  return $days % 7;
}

function fondomaria_admin_extract_special__prueba_hecha($field, $form_state, $index_prueba) {
  $field_pruebas = fondomaria_admin_get_input_field($field, $form_state);

  if($field_pruebas) {
    $prueba = $field_pruebas[$index_prueba];
    return (empty($prueba[2])) ? NULL : '1';
  }
  else {
    return NULL;
  }
}

function fondomaria_admin_extract_special__resultado_prueba($field, $form_state, $index_prueba) {
  $prueba_hecha = fondomaria_admin_extract_special__prueba_hecha($field, $form_state, $index_prueba);
  

  if($prueba_hecha) {
    $field_pruebas = fondomaria_admin_get_input_field($field, $form_state);
    $prueba = $field_pruebas[$index_prueba];
    return $prueba[2];
  }
  else {
    return NULL;
  }
}

function fondomaria_admin_extract_special__fecha_prueba($field, $form_state, $index_prueba) {
  $prueba_hecha = fondomaria_admin_extract_special__prueba_hecha($field, $form_state, $index_prueba);

  if($prueba_hecha) {
    $field_pruebas = fondomaria_admin_get_input_field($field, $form_state);
    $prueba = $field_pruebas[$index_prueba];
    $date = $prueba[4];
    return fondomaria_date_from_array($date);
  }
  else {
    return NULL;
  }
}

function fondomaria_admin_extract_special__semanas_prueba($field, $form_state, $index_prueba) {
  $prueba_hecha = fondomaria_admin_extract_special__prueba_hecha($field, $form_state, $index_prueba);

  if($prueba_hecha) {
    $field_pruebas = fondomaria_admin_get_input_field($field, $form_state);
    $prueba = $field_pruebas[$index_prueba];
    return $prueba[3];
  }
  else {
    return NULL;
  }

}

function fondomaria_admin_extract_special__orina($field, $form_state) {
  return fondomaria_admin_extract_special__prueba_hecha($field, $form_state, 1);
}

function fondomaria_admin_extract_special__result_orina($field, $form_state) {
  return fondomaria_admin_extract_special__resultado_prueba($field, $form_state, 1);
}

function fondomaria_admin_extract_special__fecha_prueba_orina($field, $form_state) {
  return fondomaria_admin_extract_special__fecha_prueba($field, $form_state, 1);
}

function fondomaria_admin_extract_special__sangre($field, $form_state) {
  return fondomaria_admin_extract_special__prueba_hecha($field, $form_state, 2);
}

function fondomaria_admin_extract_special__result_sangre($field, $form_state) {
  return fondomaria_admin_extract_special__resultado_prueba($field, $form_state, 2);
}

function fondomaria_admin_extract_special__semanas_gestacion_sangre($field, $form_state) {
  return fondomaria_admin_extract_special__semanas_prueba($field, $form_state, 2);
}

function fondomaria_admin_extract_special__fecha_prueba_sangre($field, $form_state) {
  return fondomaria_admin_extract_special__fecha_prueba($field, $form_state, 2);
}

function fondomaria_admin_extract_special__ultrasonido($field, $form_state) {
  return fondomaria_admin_extract_special__prueba_hecha($field, $form_state, 3);
}

function fondomaria_admin_extract_special__result_ultrasonido($field, $form_state) {
  return fondomaria_admin_extract_special__resultado_prueba($field, $form_state, 3);
}

function fondomaria_admin_extract_special__semanas_gestacion_ultrasonido($field, $form_state) {
  return fondomaria_admin_extract_special__semanas_prueba($field, $form_state, 3);
}

function fondomaria_admin_extract_special__fecha_prueba_ultrasonido($field, $form_state) {
  return fondomaria_admin_extract_special__fecha_prueba($field, $form_state, 3);
}

?>
